<?php
require_once 'core/init.php';
if(Input::exists()){
    if(Token::check(Input::get('token'))){
        $validate = new Validate();
        $validation = $validate->check($_POST, array(
            'username' => array(
                'required'  => true,
                'min'       => 2,
                'max'       => 20,
                'unique'    => 'users'
            ),
            'password' => array(
                'required'  => true,
                'min'       => 6,
            ),
            'password_again' => array(
                'required'  => true,
                'matches'   => 'password'
            ),
            'name' => array(
                'required'  => true,
                'min'       => 6,
                'max'       => 50
            )
        ));

        if($validate->passed()){
            $user = new User();

            $salt = Hash::salt(32);
            try{

                $user->create(array(
                    'Username' => Input::get('username'),
                    'Password' => Hash::make(Input::get('password'), $salt),
                    'Salt' => $salt,
                    'email' => ''
                ));

                Session::flash('home', 'Zarejestrowano szacun');
                Redirect::to('index.php');
            }catch (Exception $e){
                die($e->getMessage());
            }
        }else{
            foreach($validate->errors() as $error){
                echo "{$error} <br>";
            }
        }
    }
}
?>
<meta charset="utf-8">
<form action="" method="post">

    <div class="field">
        <label for="username">Nazwa użytkownika</label>
        <input type="text" name="username" id="username" value="<?php echo escape(Input::get('username')); ?>" autocomplete="off">
    </div>

    <div class="field">
        <label for="password">Hasło</label>
        <input type="password" name="password" id="password" value="" autocomplete="off">
    </div>

    <div class="field">
        <label for="password_again">Podaj hasło ponownie</label>
        <input type="password" name="password_again" id="password_again" value="" autocomplete="off">
    </div>

    <div class="field">
        <label for="name">Imię</label>
        <input type="text" name="name" id="name" value="<?php echo escape(Input::get('name')); ?>" autocomplete="off">
    </div>

    <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">

    <input type="submit" value="Rejestruj">

</form>