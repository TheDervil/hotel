<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 2015-04-10
 * Time: 22:48
 */

function escape($string){
    return htmlentities($string, ENT_QUOTES, 'UTF-8');
}