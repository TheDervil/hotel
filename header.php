<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="<?php echo Config::get('host_link');?>/Theme/css/main.css">
    <link rel="stylesheet" href="<?php echo Config::get('host_link');?>/Theme/css/jquery.datetimepicker.css">
    <script src="<?php echo Config::get('host_link');?>/Theme/js/jquery-1.11.3.min.js"></script>
    <script src="<?php echo Config::get('host_link');?>/Theme/js/jquery.datetimepicker.js"></script>
    <script src="<?php echo Config::get('host_link');?>/Theme/js/functions.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>
<body>

