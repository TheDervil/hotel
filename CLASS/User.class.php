<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 2015-04-10
 * Time: 22:46
 */

class User {
    private $_db,
            $_date,
            $_sessionName,
            $_isLoggedin;

    public function __construct($user = null){
        $this->_db = DB::getInstance();
        $this->_sessionName = Config::get('session/session_name');

        if(!$user){
            if(Session::exists($this->_sessionName)){
                $user = Session::get($this->_sessionName);
                if($this->find($user)){
                    $this->_isLoggedin = true;
                }else{
                    //
                }
            }
        }else{
            $this->find($user);
        }
    }

    public function create($fields = array()){
        if(!$this->_db->insert('user', $fields)){
            throw new Exception('problem podczas tworzenia');
        }
    }

    public function find($username = NULL){
        if($username){
            $field = (is_numeric($username)) ? 'idUser' : 'Username';
            $data = $this->_db->get('user', array($field, '=', $username));

            if($data->count()){
                $this->_date = $data->first();
                return true;
            }
        }
        return false;
    }

    public function login($username = NULL ,$password = NULL){
        $user = $this->find($username);
        if($user){
            if($this->date()->Password === Hash::make($password,$this->date()->Salt)){
                Session::put($this->_sessionName, $this->date()->idUser);
                return true;
            }
        }
        return false;
    }

    public function logout(){
        Session::delete($this->_sessionName);
    }

    public function update($fields = array(), $id= NULL){

        if(!$id && $this->isLoggedIn()){
            $id = $this->date()->id;
        }

        if(!$this->_db->update('user', $id, $fields)){
               // throw new Exception('Problem podczas aktualizacji');
        }
    }

    public function date(){
        return $this->_date;
    }

    public function isLoggedIn(){
        return $this->_isLoggedin;
    }

}