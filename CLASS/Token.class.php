<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 2015-04-10
 * Time: 22:42
 */

class Token {
    public static function generate(){
        return session::put(Config::get('session/token_name'), md5(uniqid()));
    }

    public static function check($token) {
        $tokenName = Config::get('session/token_name');

        if(session::exists($tokenName) && $token === session::get($tokenName)){
            session::delete($tokenName);
            return true;
        }

        return false;
    }
}