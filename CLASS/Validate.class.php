<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 2015-04-10
 * Time: 22:46
 */

class Validate {
    private $_passed = false;
    private $_errors = array();
    private $_db = null;

    public function __construct(){
        $this->_db =  DB::getInstance();
    }

    public function check($source, $items = array()){
        foreach($items as $item=>$rules){
            foreach($rules as $rule => $value){


                $field_value = trim($source[$item]);

                if($rule === 'required' && empty($field_value)){
                    $this->addError("<div class='error'><p>{$item} jest wymagane</p></div>");
                }else if(!empty($field_value)){
                    switch($rule){
                        case 'min':
                            if(strlen($field_value) < $value){
                                $this->addError("<div class='error'><p>{$item} to pole powinno posiadac co najmniej {$value} znaków</p></div>");
                            }
                        break;
                        case 'max':
                            if(strlen($field_value) > $value){
                                $this->addError("<div class='error'><p>{$item} to pole powinno posiadac makymalnie {$value} znaków</p></div>");
                            }
                        break;
                        case 'matches':
                            if($field_value != $source[$value]){

                                $this->addError("<div class='error'><p>{$value} musi odpowiadać {$item}</p></div>");

                            }
                        break;
                        case 'unique':
                            //$check = $this->_db->get($value, array($item,'=',$field_value));
                            //if($check->count()){
                            //   $this->addError("<div class='error'><p>{$item} już istnieje</p></div>");
                            // }
                        break;
                    }
                }


            }
        }
        if(empty($this->_errors)){
            $this->_passed = true;
        }
    }

    private function addError($error){
        $this->_errors[] = $error;
    }

    public function errors(){
        return $this->_errors;
    }

    public function passed(){
        return $this->_passed;
    }


}

