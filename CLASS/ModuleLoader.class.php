<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 2015-05-17
 * Time: 21:15
 */

class ModuleLoader{
    private $_module;
    public function __construct($module){
        $this->_module = $module;

        switch($this->_module){
            case 'rezerwacja_pokoi':
                include_once('app/rezerwacja.php');
            break;

            case 'prace-w-pokojach':
                include_once('app/prace-w-pokojach.php');
            break;

            default:
                Redirect::to('home');
            break;
        }
    }



}