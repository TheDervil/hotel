<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 2015-05-17
 * Time: 12:25
 */
require_once 'core/init.php';

class MainPage{
    private $_activePage;
    private $_user;
    public function __construct($active) {
        $this->_activePage = $active;
        $this->_user = new User();

        switch($this->_activePage){
            case 'login':
                include_once('pages/login.php');
            break;

            case 'home':
                $this ->_user->isLoggedIn() ?  include_once('pages/home.php') : Redirect::to('login');
            break;

            case 'logout':
                $this ->_user->isLoggedIn() ?  include_once('pages/logout.php') : Redirect::to('login');
            break;

            default:
                include_once('pages/login.php');
            break;

        }
    }


}