<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 2015-04-11
 * Time: 10:56
 */

class Config{
    public static function get($path = null){
        if($path){
            $config = $GLOBALS['config'];
            $path = explode('/', $path);
            foreach($path as $bit){
                if(isset($config[$bit])){
                    $config = $config[$bit];
                }
            }
            return $config;
        }
        return false;
    }
}