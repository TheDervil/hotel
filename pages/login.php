<div class="sections-full content login">
    <?php
        require_once 'core/init.php';
        $err = false;
        $re = new User();
        if($re->isLoggedIn()){Redirect::to('home');}
        if(Input::exists()){
            if(Token::check(Input::get('token'))){
                $validate = new Validate();

                $validate->check($_POST, array(
                    'username' => array('required' => true),
                    'password' => array('required' => true)
                ));

                if($validate->passed()){

                    $user = new User();
                    $login = $user->login(Input::get('username'), Input::get('password'));

                    if($login){
                        Redirect::to('index.php');
                    }else{
                        echo "<div class='error'><p>Problem z logowaniem</p></div>";
                        $err = true;

                    }

                }else{
                    foreach($validate->errors() as $error){
                        echo $error;
                    }
                }
            }
        }
    ?>
    <div class="content-log">
        <img src="Theme/images/login_logo.png" alt="hotel_manager">
        <form action="" method="post">
            <div class="field">
                <input type="text" name="username" id="username" autocomplete="off" placeholder="Login" value="<?php echo Input::get('username') ?>">
            </div>

            <div class="field">
                <input type="password" name="password" id="password" autocomplete="off" placeholder="Hasło">
            </div>
            <?php if($err){echo 'Login lub hasło nie poprawne';} ?>

            <input type="hidden" name="token" value="<?php echo Token::generate() ?>">
            <input class="logIn-btn" type="submit" value="Log In">
        </form>
    </div>

</div>

