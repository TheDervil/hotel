<?php
$user = new User();
?>
<header class="mainPageH">
    <div class="logo">
        <img src="<?php echo Config::get('host_link');?>/Theme/images/logo.png" alt="Hotel Manager">

        <h2>HOTEL MANAGER</h2>
    </div>
    <div class="profile-nav">
        <img alt="avatar" src="<?php echo Config::get('host_link');?>/Theme/images/avatar.png">
        <ul>
            <li id="user-nav"><a href="#"><p>Witaj <?php echo $user->date()->Username; ?></p></a>
                <ul class="submenu-usr">
                    <li><a href="<?php echo Config::get('host_link');?>/logout">Wyloguj</a></li>
                </ul>
            </li>

        </ul>

    </div>
</header>
<div class="sections main-page">
    <div class="nav-col side-nav">
        <ul>
            <li><a href="<?php echo Config::get('host_link') ?>/home">
                    <div class="ico-cont-nav"><img src="<?php echo Config::get('host_link');?>/Theme/images/panel-ico.png" alt=""></div>
                    <span>Panel główny</span></a></li>
            <li><a href="<?php echo Config::get('host_link');?>/home/rezerwacja-pokoi">
                    <div class="ico-cont-nav"><img src="<?php echo Config::get('host_link');?>/Theme/images/rezerwacja-ico.png" alt=""></div>
                    <span>Rezerwacja pokoi</span></a></li>
            <li><a href="#">
                    <div class="ico-cont-nav"><img src="<?php echo Config::get('host_link');?>/Theme/images/meldunek-ico.png" alt=""></div>
                    <span>Meldunek gości</span></a></li>
            <li><a href="#">
                    <div class="ico-cont-nav"><img src="<?php echo Config::get('host_link');?>/Theme/images/rezerwacja-ico.png" alt=""></div>
                    <span>Rezerwacje/meldunki</span></a></li>
            <li><a href="<?php echo Config::get('host_link');?>/home/prace-w-pokojach">
                    <div class="ico-cont-nav"><img src="<?php echo Config::get('host_link');?>/Theme/images/praca-ico.png" alt=""></div>
                    <span>Prace w pokojach</span></a></li>
            <li><a href="#">
                    <div class="ico-cont-nav"><img src="<?php echo Config::get('host_link');?>/Theme/images/baza-ico.png" alt=""></div>
                    <span>Baza firm</span></a></li>
            <li><a href="#">
                    <div class="ico-cont-nav"><img src="<?php echo Config::get('host_link');?>/Theme/images/dokumenty-ico.png" alt=""></div>
                    <span>Dokumenty</span></a></li>
            <li><a href="#">
                    <div class="ico-cont-nav"><img src="<?php echo Config::get('host_link');?>/Theme/images/pracownicy-ico.png" alt=""></div>
                    <span>Pracownicy</span></a></li>
            <li><a href="#">
                    <div class="ico-cont-nav"><img src="<?php echo Config::get('host_link');?>/Theme/images/grafik-ico.png" alt=""></div>
                    <span>Grafik</span></a></li>
        </ul>
    </div>
    <div class="wrapper">

        <?php
            if(isset($_GET['app'])){
                switch($_GET['app']){
                    case 'rezerwacja-pokoi':
                        $mod = new ModuleLoader('rezerwacja_pokoi');
                    break;
                    case 'prace-w-pokojach':
                        $mod = new ModuleLoader('prace-w-pokojach');
                        break;
                    default:
                        echo 'blank';
                    break;
                }

            }
        ?>

    </div>
</div>