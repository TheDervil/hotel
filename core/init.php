<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 2015-04-11
 * Time: 00:04
 */

session_start();

$GLOBALS['config'] = array(
    'mysql' => array(
        'host' => '127.0.0.1', //urzywajcie 127.0.0.1 a nie localhost szybciej działa :)
        'username' => 'root',
        'password' => '',
        'db' => 'hotel_manager'

    ),
    'remember' => array(
        'cookie_name' => 'hash',
        'cookie_expiry' => 60*60*24*7
    ),
    'session' => array(
        'session_name' => 'user',
        'token_name' => 'token'
    ),
    'host_link' => $actual_link = "http://$_SERVER[HTTP_HOST]/hotel"

);

//spl - standard php lib

spl_autoload_register(function($class){
    require_once 'CLASS/' . $class . '.class.php';
});

require_once 'Functions/sanitize.php';