<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 2015-05-17
 * Time: 11:56
 */

require_once 'core/init.php';
require_once 'header.php';

$tb = DB::getInstance();


$count = 0;

$user = new User();


if($user->isLoggedIn()){
    if(!isset($_GET['page'])){
        Redirect::to('home');
    }
}else {
    if(!isset($_GET['page'])){
        Redirect::to('login');
    }
}

$mp = new MainPage($_GET['page']);



require_once 'footer.php';